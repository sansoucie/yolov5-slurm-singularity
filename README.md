# Portable HPC yolov5

## Installation
Install slurm and modulefiles. Set up a modulefile `singularity/3.7`.
Alternatively, use the provided `install-singularity` script to install singularity on a local computer

## Setup
singularity remote login

wandb login

## Usage

The `run` script contains entrypoints for all actions 

`run build`: Build the yolov5 singularity image, and push it to the cloud

`run pull`: Pull the yolov5 singularity image from the cloud

`run install`: Install Go and Singularity

`run train`: Train a yolov5 model

`run resume`: Resume a stopped training run

`run detect`: Use a pretrained model to detect objects in images

`run evolve`: Hyperparameter optimization

## Training set structure
One directory, containing two subdirectories (`images` and `labels`), as well as three files (`obj.names`, `train.txt`, `valid.txt`). `obj.names` is a list of class names. `train.txt` is a list of image filenames for the training set, and `valid.txt` is the same for the validation set. Each image should be listed as a filename (no directories), and there should be a corresponding `.txt` file in `labels` with one line per bounding box, given as `class x y w h`.

## sbatch.env
Copy the `sbatch.env.template` file to `sbatch.env`, and adjust the values as needed. 
